(function() {
  var selectedValue = 0;
  $('#select-department').on('change', function() {
    selectedValue = this.value;

    if (selectedValue === 'all') {
      $('h4.department-title, h5.team-title, div.job-container').removeClass('hidden');
      $('div.jobs-counter').addClass('hidden');
      showJobs();
    } else {
      $('div.job-container, div.jobs-counter').removeClass('hidden');
      $('h4.department-title, h5.team-title').addClass('hidden');
      $('div.job-container').addClass('hidden');
      // refactor to only change margin of dataSelection
      showJobs(selectedValue, 0);
      // must remain last line as total counter of all visible jobs
      $('div.jobs-counter #counter').text($('div.job-container:visible').length + ' jobs');
    }
  });

  var resizeMargin = function(depth) {
    // use view defined depth or element's own depth value based on hierarchy
    var depthValue = (depth === -1) ? $(this).data('depth') : depth;
    $(this).css({'margin-left': parseInt(depthValue) * 10});
  };

  function showJobs(selectedValue = 0, depth = -1) {
    var dataTeams;
    $('div.job-container').each(function() {
      dataTeams = $(this).attr('data-teams') || null;
      // selected value id from dropdown must be greater than 0
      if (selectedValue > 0) {
        if (dataTeams && dataTeams.includes(selectedValue)) $(this).removeClass('hidden');
        // we want different `this` context, using only shown jobs elements
        resizeMargin.call(this, depth);
      } else {
        // use each job-container for resize by default
        resizeMargin.call(this, depth);
      }
    });
  }
})();
