- name: Infrastructure Average Location Factor
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously, and hiring great people in low-cost regions where we pay market rates. We track an average location factor by function and department so managers can make tradeoffs and hire in an expensive region when they really need specific talent unavailable elsewhere, and offset it with great people who happen to be in low cost areas.
  target: equal or less than 0.58
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are currently above our target after the shift of location factor to geo regions. We are also doing significant hiring and have focused part of our hires in APAC, allowing opportunities for some more efficient geo region hires.
  sisense_data:
    chart: 11490180
    dashboard: 851333
    embed: v2
- name: Infrastructure Budget Plan vs Actuals
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a responsible business to be successful, and to one day go on the public market.
  target: Identified in Sisense Chart
  org: Infrastructure Department
  is_key: false
  health:
    level: 0
    reasons:
    - Data now lives in adaptive for FY22, not in data warehouse. This is a Q2 priority for data team
- name: Infrastructure Handbook MR Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/index.html#infrastructure-handbook-update-frequency"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-mr-rate"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching `/source/handbook/engineering/**` or `/source/handbook/support/**` over time.
  target: .55
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - Adjusted the target to .55 to be consistent with larger org, reflect less activity from managers, and overall the trend that our initial suggested target is higher than many months of observed activity.
  sisense_data:
    chart: 10586746
    dashboard: 621063
    shared_dashboard: b69578ca-d4a6-4a99-b06f-423a3683446c
    embed: v2
- name: Infrastructure Hiring Actual vs Plan
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "infrastructure".
  target: greater than 0.9
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - Hiring for SRE Engineering Managers remains challenging and competitive.
    - Hiring for DBRE remains challenging. We have seen better recent results after adding another engineer to the screening process and are currently onboarding an agency.
    - 1 SRE started in May, and additional is set to start in June.
    - In April 1 DBRE position was moved to on hold to fund 1 Trust & Safety team position.
    - Currently hiring for 9 positions (2x DBRE, 4x SRE, 3x SRE Manager).
  sisense_data:
    chart: 11488988
    dashboard: 851270
    embed: v2
  urls:
  - "/handbook/hiring/charts/infrastructure-department/"
- name: Infrastructure MR Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-mr-rate"
  definition: Infrastructure Department MR is a performance indicator showing how many changes the Infrastructure team implements in the GitLab product, 
    as well as changes in support of the GitLab SaaS infrastructure. We currently count all members of the Infrastructure Department (Directors, Managers, ICs) 
    in the denominator, because this is a team effort. The full definition of MR Rate is linked in the url section.
  target: Greater than 6 MRs per month
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - We intend to observe this metric over time to understand the behavior of the differing work patterns and MR content (product code + infrastructure logic + configuration data).
  urls:
    - "/handbook/engineering/metrics/#merge-request-rate"
  sisense_data:
    chart: 8934544
    dashboard: 686934
    shared_dashboard: 178a0fbc-42a9-4181-956d-a402151cf5d8
    embed: v2
- name: Infrastructure New Hire Average Location Factor
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously, and hiring great people in low-cost regions where we pay market rates. We track an average location factor for team members hired within the past 3 months so hiring managers can make tradeoffs and hire in an expensive region when they really need specific talent unavailable elsewhere, and offset it with great people who happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: 0.58
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - Recent new hires have had slightly upward pressure on the metric. A couple of our pending roles for hire will provide opportunity for more efficient geo zone hires in APAC.
  sisense_data:
    chart: 9389296
    dashboard: 719549
    embed: v2   
- name: Infrastructure Department Discretionary Bonus Rate
  parent: "/handbook/engineering/performance-indicators/#engineering-discretionary-bonus-rate"
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
      - Metric is new and is being monitored
  sisense_data:
    chart: 11860249
    dashboard: 873088
    embed: v2
    filters:
      - name: Breakout
        value: Department
      - name: Breakout_Division_Department
        value: Engineering - Infrastructure
- name: GitLab.com Availability
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Percentage of time during which GitLab.com is fully operational and providing service to users within SLO parameters. Definition is available on the <a href="https://gitlab.com/gitlab-com/dashboards-gitlab-com/-/metrics/sla-dashboard.yml?environment=1790496&duration_seconds=2592000">GitLab.com Service Level Availability page</a>.
  target: equal or greater than 99.95%
  org: Infrastructure Department
  is_key: true
  urls:
  - https://about.gitlab.com/handbook/engineering/monitoring/#gitlabcom-service-level-availability 
  - https://about.gitlab.com/handbook/engineering/monitoring/#historical-service-level-availability
  - https://dashboards.gitlab.net/d/general-slas/general-slas?orgId=1&from=now-30d&to=now
  - https://gitlab.com/gitlab-com/dashboards-gitlab-com/-/metrics/sla-dashboard.yml?environment=1790496&duration_seconds=2592000
  health:
    level: 2
    reasons:
    - May 2021 Availability 99.85%
    - Historical Availability is available on the <a href="https://about.gitlab.com/handbook/engineering/monitoring/#historical-service-level-availability">Service Level Availability page</a>
  sisense_data:
    chart: 11526524
    dashboard: 853539
    embed: v2
- name: Mean Time To Production (MTTP)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the elapsed time (in hours) from merging a change in <a href="https://gitlab.com/gitlab-org/gitlab">gitlab-org/gitlab projects</a> master branch, to deploying that change to gitlab.com. It serves as an indicator of our speed capabilities to deploy application changes into production.
  target: less than 24 hours
  org: Infrastructure Department
  is_key: true
  urls:
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/170
  health:
    level: 3
    reasons:
    - This metric is equivalent to the <i>Lead Time for Changes</i> metric in the <a href="https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance">Four Keys Project from the DevOps Research and Assessment</a>. Additionally, the data for this metric also shows <i>Deployment Frequency</i>, another of the Four Keys metrics.
    - Due to continued increased deployment delays continuing from previous months, in March 2021 the target was increased to 24 hours. We will <a href="https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/65">consider lowering the target</a> once we are able to deploy without any interruptions for two consecutive weeks. At this time we have had one week without interruptions and will be reviewing the target at the end of this week (w/c 17th May).
    - MTTP increases in Jan. and Feb. 2021 are a result of the <a href="https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl">PCL</a> and <a href="https://gitlab.com/gitlab-com/gl-infra/production/-/issues/3487"> deployment-impacting migration timeouts</a>.
    - In September 2020, the target is lowered to 12 hours (previously 24).
    - The next step for this metric is to move from Continuous Delivery to Continuous Deployment for GitLab.com. Work is tracked in <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/280"> epic 280 </a>.
  sisense_data:
    chart: 10055732
    dashboard: 764878
    embed: v2
- name: Mean Time Between Failure (MTBF)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the average time (in hours) between failures for any service. Higher values are better. Failure is a breach of the MTBF threshold which is higher than our Production monitoring threshold, and in turn, higher than our contractual SLO thresholds. MTBF is an internal system measurement used as an indicator on whether focus is necessary for individual services, used in combination with <a href="/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-individual-service-maturity"> GitLab.com service maturity </a>. MTBF is not a measure of the customer perceived impact, for that see <a href="/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability">GitLab.com Availability</a> and <a href="/handbook/engineering/infrastructure/performance-indicators/#mean-time-between-incidents-mtbi"> MTBI</a>.
  target: 6 hours.
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/941
  - https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/876
  - https://gitlab.com/gitlab-data/analytics/-/issues/7713
  - https://dashboards.gitlab.com/d/general-mtbf/general-mean-time-between-failure?orgId=1
  chart_image: https://gitlab.com/api/v4/projects/14231319/jobs/artifacts/master/raw/global-gitlab-com-mtbf.png?job=refresh-mtbf-graph
  health:
    level: 2
    reasons:
    - The initial target is set in March 2021.
    - Chart image from Grafana is used because <a href="https://gitlab.com/gitlab-data/analytics/-/issues/7713#note_524683371">we are not able to add Thanos as a datasource for Sisense</a>.
    - Metric is update to reflect new method of using multi-window-multi-burn rates.
    - Discussion continues in <a href="https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/941">the issue.</a>
    - This metric was re-introduced in October 2020.
- name: Mean Time Between Incidents (MTBI)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the mean elapsed time (in hours) from the start of one production incident, to the start of the next production incident. It serves primarily as an indicator of the amount of disruption being experienced by users and by on-call engineers. This metric includes only <a href="https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability">Severity 1 & 2 incidents</a> as these are most directly impactful to customers. This metric can be considered "MTBF of Incidents".
  target: more than 120 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/764913/MTBF-KPI-Dashboard?widget=10056392&udv=0
  health:
    level: 1
    reasons:
    - Target at 120 hour with the intent that we should not have such incidents more than approximately weekly (hopefully less).  Furter iterations will increase this target when we incorporate environment (production only).
    - Deployment failures (and the mean time between them) will be extracted into a separate metric to serve as a quality countermeasure for MTTP, unrelated to this metric which focuses on declared service incidents.
  sisense_data:
    chart: 10056392
    dashboard: 764913
    embed: v2
- name: GitLab.com Hosting Cost per GitLab.com Unique Monthly Active Users
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support one user in GitLab.com. It is an important metric because it allows us to estimate infrastructure costs as our user base grows. GitLab.com   Hosting Cost comes from Netsuite; it is a sum of actual amounts within GitLab.com:Marketing and GitLab.com departments, with account numbers of 5026 or 6026. This cost is divided by <a href="/handbook/product/performance-indicators/#unique-monthly-active-users-umau">UMAU</a>
  target: less than .80
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/764957/Infrastructure-Hosting-Cost-UMAU-KPI
  health:
    level: 2
    reasons:
    - See notes in Key Agenda Doc.
- name: GitLab.com Hosting Cost per Free GitLab.com Unique Monthly Active Users
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support one user of GitLab.com at the Free tier. GitLab.com  Hosting Cost comes from Netsuite; it is a sum of actual amounts within GitLab.com:Marketing department, with account numbers of 5026 or 6026. This cost is divided by GitLab.com Free UMAU (Total UMAU minus total paid licensed users).
  target: less than .50
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/764957/GitLab.com-Hosting-Cost-UMAU-KPI
  health:
    level: 2
    reasons:
    - See notes in Key Agenda Doc.
- name: GitLab.com Hosting Cost per Paid GitLab.com Licensed User
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support one user of GitLab.com at the paid tiers. GitLab.com Hosting Cost comes from Netsuite; it is a sum of actual amounts within GitLab.com department, with account numbers of 5026 or 6026. This cost is divided by total paid licensed users on GitLab.com.
  target: less than 3
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/764957/GitLab.com-Hosting-Cost-UMAU-KPI
  health:
    level: 2
    reasons:
    - See notes in Key Agenda Doc.
- name: Infrastructure Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Infrastructure Department
  is_key: yes
  public: false
  health:
    level: 2
    reasons:
    - downward trend, getting close to below target
  urls:
    - "https://app.periscopedata.com/app/gitlab/861470/Infrastructure-Department-Retention" 
- name: Infrastructure Time to Fill Vacancies
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-time-to-fill-vacancies"
  definition: We need to be able fill open positions in a relatively timely manner. Time to Fill measures how long it takes for an job opening to be closed on average. It is distinct from Time to Hire or Time to Offer Accept metrics as this metric measures the full pipeline from sourcing all the way to closing the position due to candidate accepting an offer. Time to Hire/ Time to Offer Accept only measures the time from when a candidate applies to when they accept, so it does not include sourcing time.
  target: at or below 50 days
  org: Infrastructure Department
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - new metric, monitoring progress
  sisense_data:
      chart: 11885848
      dashboard: 872394
      embed: v2
  filters:
      - name: Division
        value: Engineering
      - name: Department
        value: Infrastructure

- name: GitLab.com individual service maturity
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric aims to show the level of confidence we have of our insight into the significant GitLab services on GitLab.com. We are looking to capture the maturity on the scale of "manual check is necessary" to "the system informs us of an upcoming scaling need". This PI is paired with MTBF. For a service of highest maturity, decreased MTBF will show a need for additional scaling work on the service. For a service of lowest maturity, increased MTBF will require work on raising the maturity of the service.
  target: TBD
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/296
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/382
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/398
  health:
    level: 0
    reasons:
    - We are automating the data collection to simplify the process. Details are in <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/398">this epic</a>.
    - In January 2021, we began to implement a Service Maturity Model. The effort is manual and is being <a href="/handbook/engineering/infrastructure/team/scalability/#maturity-model">tracked in the handbook</a>, before introducing a target and associated metric.
    - In October, November and December 2020, we found that the missing part of tracking the scaling bottlenecks is the understanding of the service maturity and how the service contributes to system failures. We introduced MTBF in October as a counterpart to service maturity.
- name: Mean Time To Resolution (MTTR)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: For all <a href="/handbook/engineering/monitoring/#gitlabcom-service-level-availability"> customer-impacting services</a>, measures the elapsed time (in hours) it takes us to resolve when an incident occurs. This serves as an indicator of our ability to execute said recoveries. This includes Severity 1 & Severity 2 incidents from <a href="https://gitlab.com/gitlab-com/gl-infra/production">production project</a>
  target: less than 24 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/gl-infra/production/-/boards/1717012?&label_name[]=incident
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8296
  - https://app.periscopedata.com/app/gitlab/766694/MTTR-KPI
  health:
    level: 2
    reasons:
    - data depends on SREs adding incident::resolved label
  sisense_data:
    chart: 10083860
    dashboard: 766694
    embed: v2
- name: Mean Time To Mitigate (MTTM)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: For all <a href="/handbook/engineering/monitoring/#gitlabcom-service-level-availability"> customer-impacting services</a>, measures the elapsed time (in hours) it takes us to mitigate when an incident occurs. This serves as an indicator of our ability to mitigate production incidents. This includes Severity 1 & Severity 2 incidents from <a href="https://gitlab.com/gitlab-com/gl-infra/production">production project</a>
  target: less than 1 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/766694/MTTR-KPI
  health:
    level: 2
    reasons:
    - This metric is equivalent to the <i>Time to Restore</i> metric in the <a href="https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance">Four Keys Project from the DevOps Research and Assessment</a>
    - data depends on SREs adding incident::mitigate label
  sisense_data:
    chart: 10083556
    dashboard: 766694
    embed: v2
- name: Corrective Action SLO
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: The Corrective Actions (CAs) SLO focuses on the number of open severity::1/severity::2 Corrective Action Issues past their due date. All Corrective Actions in groups under GitLab.org and GitLab.com groups are in scope, regardless of department or project. CAs and their due dates are defined in our <a href="/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership">Incident Review process</a>.
  target: below 10
  org: Infrastructure Department
  is_key: false
  health:
    level: 1
    reasons:
    - Due to the growing backlog of Corrective Actions, we are shifting towards more tech debt work in FY22, starting with OS and instance upgrades.
    - We started tracking this in July 2020. Many of our Corrective Actions do not have a proper Severity assigned yet (137 as of 2021/04/15)
    - Number of open past due S1/S2 corrective actions has been growing 
  urls:
    - https://about.gitlab.com/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership
    - https://app.periscopedata.com/app/gitlab/787921/Corrective-Actions
  sisense_data:
    chart: 11500976
    dashboard: 852027
    embed: v2
- name: GitLab.com Saturation Forecasting
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: It is critical that we continuously observe resource saturation normal growth as well as detect anomalies. This helps to ensure that we have the appropriate platform capacity in place. This metric uses the saturation monitoring framework historical data to then forecast future expectations. Where the forecasted future expectation crosses a preset days-til and confidence threshold this metric will count a single violation per service. This metric will focus on non-horizontally scalable services.
  target: 0
  org: Infrastructure Department
  is_key: false
  health:
    level: 0
    reasons:
    - New PI as of May 2021. Development of this metric in progress.
  urls: 
    - "https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/63"
    - "https://gitlab.com/gitlab-com/gl-infra/tamland/-/merge_requests/19"
    - "https://thanos-query.ops.gitlab.net/graph?g0.range_input=1h&g0.max_source_resolution=0s&g0.expr=tamland_forecast_violation_days&g0.tab=0"
- name: GCP CUD Coverage %
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Committed Use Discounts allow us to lower the rate we pay for Compute by comitting to a certain level of usage for a set amount of time. We aim to cover at least 80% of our total GCP Compute usage with Committed Use Discounts to optimize our spend
  target: at or above 80%
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - over 80% 
  urls:
    - "https://cloud.google.com/compute/docs/instances/signing-up-committed-use-discounts"
    - https://app.periscopedata.com/app/gitlab/848796/WIP:-GCP-CUD-Overview
  sisense_data:
    chart: 11508863
    dashboard: 848796
    embed: v2
- name: GitLab.com Hosting Cost / Revenue
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: We need to spend our investors' money wisely. As part of this we aim to follow industry standard targets for hosting cost as a % of overall revenue. In this case revenue is measured as MRR + one time monthly revenue from CI & Storage
  target: TBD
  public: false
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - See Infra Key Meeting Agenda for details
  urls:
    - "https://app.periscopedata.com/app/gitlab/828459/WIP:-GitLab.com-Hosting-Cost-Revenue-KPI"
