---
layout: handbook-page-toc
title: "Focus Account Lists"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Focus Account Lists
At GitLab we have a Focus Account List (FAL) for both our Large and Mid-Market segments.  These lists account for our `Account Centric` marketing motion.  Our focus account lists will always include our [ICP accounts](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-strategy/ideal-customer-profile/) but because priorities shift and we may choose to focus on different developer counts, industries, regions etc the definition of our FAL will change periodically.  The up to date definition will be documented below.  Additionaly, we are currently aligned to sales goals and the percentage of accounts per region will be reflected as such.  This is not a true intent or data driven model because, for example, there may be lower overall intent in APAC versus US West, however from a marketing perspective, because we are aligned to sales goals, we will have accounts in FAL that have a lower intent scoring that the accounts in US West.

### How we develop our focus account lists
We take from a variety of data sets currently but the ideal state is to have all data flowing to Salesforce as our single source for qualifying and scoring accounts.  The overarching goal of these lists is to amplify efforts across the entire GitLab team to win these accounts.

### GL4300 FAL criteria
GL4300 stands for GitLab & the number of focus accounts we are marketing to. This list represents only those accounts in our [`Large` segment](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation).  
1. `first order available` accounts that are marketing qualified.  This includes accounts that have hit a certain threshold of engagement with GitLab.
1. `new logo target account` accounts that are identified in SFDC.  These accounts are a subset of our `first order` total addressable market that are eligible for the `new logo` additional companies.
1. All ABM account are by default included in the FAL.
1. Sales nominated- sales flags priority accounts in SFDC using the `account rank` field.  Any `first order available` account that is also ranked by sales will be included in the FAL. Instructions to do this for your accounts is [here](/handbook/sales/field-operations/gtm-resources/#account-ranking-for-enterprise-sales).  **the exception to this rule is Public Sector because of how these account hierarchies are set up.  For PubSec, we include ranked accounts but do not apply the `first order available` requirement for these accounts.

### MM4000 FAL criteria
The focus list for the Mid Market segment.  The current MM4000 list is based on a true intent based model and does not follow regional sales goals.

MM4000 stands for MidMarket & the number of focus accounts we are marketing to. This list represents only those accounts in our [`MidMarket` segment](/handbook/sales/field-operations/gtm-resources/#segmentation).

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of employees | >500 |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket or Subversion.  Also include a LACK of tech stack in smaller companies |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization)  |
| **Additional criteria (attributes to further define)** | New hire | CIO | 
| | High intent account | Account is trending as high intent based on our data in Demandbase 

## Account hierarchy
**If a subsidiary of a conglomerate is a customer, is the parent (and all other subsidiaries) considered a customer (i.e. not net new logo)?**
Yes, as our 2H plan is focused on NEW FIRST ORDER therefore if a child account is a customer, then an account would not be included. These would be considered Connected New.
## Connected New Customer / Net New Logo
A connected new customer (sometimes called net new logo) is the first new subscription order with an Account that is related to an existing customer Account Family (regardless of relative position in corporate hierarchy) and the iACV related to this new customer is considered "Connected New".

There is a field called `Order Type` in Salesforce that on the back end automatically captures New - First Order, Connected New, and Growth. Marketing is currently focused on increasing New - First Order.

### Data sources
We use a variety of data sources to determine if an account matches one of our ideal customer profile data points.  The table below shows the order of operation of where we look for this data.

| Attribute | Data Sources (in order of priority) |
| ------ | ------ |
| Number of developers | Aberdeen number of developers --> user/SAL input in Salesforce --> No. of employees as a proxy |
| Technology stack | Gainsight input --> Aberdeen technology stack --> user/SAL input in Salesforce --> Zoominfo tech stack  |
| Cloud provider | Aberdeen technology stack --> user/SAL input in Salesforce --> Zoominfo tech stack |
| Prospect | Total CARR for all accounts within the hierarchy equals zero |

### When an account moves to/from our GL4300 & MM4000 focus account lists
Both Focus lists will be reviewed and modified during the last 2 weeks of each quarter.  The process is as follows:
##### T-minus two weeks from end of quarter
1. ABM nominations are made by sales

##### Last day of quarter
1. Closed won logos will be filtered out and the lists will be refilled based on next up ideal customer profile accounts. 
1. Accounts not showing an increase or are showing a decrease in engagement will be removed from the Focus list
1. Any additional accounts identified by sales as not a good prospect due to lack of budget, etc

##### First business day of new quarter
1. Salesforce is updated with all changes to the focus acccount list and shared with the organization


## Accounts are identified in Salesforce by the GTM strategy field in Salesforce:

**Volume**
Default selection for all accounts that are not currently in an `account centric` or `abm` motion.

**TOTAL ADDRESSABLE MARKET**
Total addressable market for `First Order` logos currently in top of funnel stages.

**ACCOUNT CENTRIC**
Indicates that an account is a focus for field marketing and account centric (1:many) campaigns (GL4300 and MM4000).

**ABM**
All accounts that are currently in our ABM programs (1:1 and 1:few).

## Account Sources
All accounts in Salesforce that are part of the `ICP Total Addressable Market` will also have the `New Logo Target Account` field completed.

**Existing** An account that already existed in our circumstances

**Core** Newly identified core user

**Lookalike** Account identified based on attributes that match our exisitng customer base

### Aberdeen Data
As part of the development of our ideal customer profile, we purchased data from [Aberdeen](https://www.aberdeen.com/?gclid=Cj0KCQiAqdP9BRDVARIsAGSZ8AlzfX6vYnVNh7YX2IKrc6uNhqjfGY6sQywcyZalJScxTyexilB0pa4aAvFdEALw_wcB) to help us determine our ICP total addressable market.  The data included number of developers, specific technologies installed, and cloud provider.  The data is rolls up to the `Ultimate Parent` as we are looking for both the best entry point for an account and the overall environment.

| Data point | Salesforce field | Description & how to use the data |
| ------ | ------ | ------ |
| Number of developers | `Aberdeen Ultimate Parent Developer Count`  | This number is the total number of current developer contacts that Aberdeen has in their database for all sites of a company.  Because it is impossible to have a database of ALL contacts at a company, we look to this data point to verify if an account has over 500 developers IF the account has a number >500 in this field but we don't exclude an account from our TAM if thecount is lower than 500 due to the nature of the data point, rather, we go to our next best data point to verify. |
| Competitive technology | `Aberdeen Ultimate Parent Technology Stack` | This field identifies if a company has a certain technology in their technology stack that is part of our ideal customer profile |
| Cloud provider | `Aberdeen Ultimate Parent Cloud Provider` | Tells us if an account has AWS, GCP, or both as their cloud provider. |

**What does the Demandbase intent score mean?**
[Demandbase handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/)
You can read all about Demandbase and how they score accounts (both numerically and by H/M/L) in the handbook page dedicated to Demandbase.

**As we run our campaigns against this list and learn more about which accounts are engaging/not, is there a process to take this learning into account for removing/adding accounts to the list?**
The GL4300 and MM4000 are both dynamic audiences. When an account is closed won/disqualified, it will be dropped from the list and refilled 1x a quarter ahead of sales QBR's.

**For accounts outside the list that are engaging, how can we share that information so they can get added to the list?**
We are using Demandbase to look track all accounts that fit our ideal customer profile.  We can see an increase in engagement in the platform and will be adding to the list quarterly.

**At a high level, how does the intent data get collected?**
[Handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/#intent-fields--definitions) 

## Where can I see the GL4300 and MM4000 account lists?
Both the GL4300 and MM4000 are identified in Salesforce by the `GTM Strategy` field = `Account Centric`. From the you can filter by segment to see the respective lists, or by account owner etc to filter further.

## How do I surface an account for review to be added to the `Focus Account Lists`?
Because we do not have all of the data possible in Salesforce, some accounts are not surfaced through our process for refilling the FAL's quarterly.  If you have an account that should be reviewed, please ensure the following is complete in Salesforce, then chatter @emilyluehrs to flag the account for review.
- number of developers (`potential users verify` field)
- technology stack is up to date (please use the Gainsight `stage technology` fields to do this)
- account hierarchy is up to date
